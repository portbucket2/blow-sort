using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class Net : MonoBehaviour
{
    [SerializeField] BallColor netColor;
    [SerializeField] GameObject ballPrefab;
    [SerializeField] Transform startingPoint;
    [SerializeField] int row = 10;
    [SerializeField] int column = 3;
    [SerializeField] float scale = 0.2f;
    [SerializeField] ParticleSystem burst;
    [SerializeField] MeshRenderer netColorRen;
    [SerializeField] Color startColor;
    [SerializeField] Color changeColor;

    [Header("Floating Text: ")]
    [SerializeField] TextMeshPro text;
    [SerializeField] float transitionTime;
    [SerializeField] float distance;
    [SerializeField] float comboTime;


    [Header("Debug: ")]
    [SerializeField] float comboTimer = 0f;
    [SerializeField] bool comboStarted = false;
    [SerializeField] int comboCount = 0;
    [SerializeField] Animator animator;
    [SerializeField] List<Vector3> positions;
    int positionCount = 0;
    float radius = 0;
    float yValue = 0;
    readonly string BALL = "ball";
    Vector3 punch;
    GameController gameController;

    WaitForSeconds WAIT = new WaitForSeconds(0.5f);
    
    void Start()
    {
        yValue = text.transform.localPosition.y;
        animator = GetComponent<Animator>();
        punch = new Vector3(1f, scale, 1f);
        gameController = GameController.GetController();
        radius = ballPrefab.GetComponent<SphereCollider>().radius * 2f;
        InitCollectionPositions();
    }
    private void Update()
    {
        if (comboTimer > 0)
        {
            comboStarted = true;
            comboTimer -= Time.deltaTime;
        }
        else
        {
            EndCombo();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag(BALL))
        {
            
            Ball ball = collision.transform.GetComponent<Ball>();
            // collect
            burst.Play();
            //ball.Collect(positions[positionCount]);
            ball.CollectVanish(startingPoint.position);
            positionCount++;
            //transform.DOKill();
            //transform.localScale = Vector3.one;
            gameController.AddBall(netColor);
            animator.SetTrigger("move");
            FloatingText();
            NetColorChange();
            if (ball.GetColor() == netColor)
            {
                // same color               
                
                //transform.DOPunchScale(punch, 0.2f, 1, 1f);\
            }
        }
    }

    void InitCollectionPositions()
    {
        positions = new List<Vector3>();
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < column; j++)
            {
                positions.Add(startingPoint.position + new Vector3(j * radius, i * radius, 0f));
            }
        }
        //positions.RemoveAt(0);
        //positions.RemoveAt(21);
    }
    void FloatingText()
    {
        comboTimer += comboTime;
        comboCount++;
        if (comboCount > 1)
        {
            text.text = comboCount + "X";
            text.transform.DOKill();
            text.transform.DOLocalMoveY(yValue, 0.01f);
            text.transform.DOLocalMoveY(yValue + distance, transitionTime).SetEase(Ease.InSine);
            text.transform.DOScale(1f, 0.01f);
            text.transform.DOScale(0.01f, transitionTime).SetEase(Ease.OutSine);
        }
        
    }
    void EndCombo()
    {
        if (comboStarted)
        {
            comboStarted = false;
            comboCount = 0;
        }
    }
    void NetColorChange()
    {
        netColorRen.material.DOKill();
        netColorRen.material.DOColor(changeColor, 0.3f).SetEase(Ease.OutSine).OnComplete(ColorChangeComplete);
    }
    void ColorChangeComplete()
    {
        netColorRen.material.DOColor(startColor, 0.3f).SetEase(Ease.OutSine);
    }
}
public enum BallColor
{
    Red,
    Blue,
    Green
}
