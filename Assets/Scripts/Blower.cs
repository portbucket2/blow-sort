using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blower : MonoBehaviour
{
    [SerializeField] float forceMult;
    [SerializeField] float upForceMult;
    [SerializeField] ParticleSystem windParticle;
    readonly string BALL = "ball";
    Rigidbody targetBody;
    Vector3 dir;
    float dirY;
    bool blowerOn = false;
    BoxCollider col;
    private void Awake()
    {
        col = GetComponent<BoxCollider>();
    }
    void Start()
    {
        
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            blowerOn = true;
            windParticle.Play();
            col.enabled = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            blowerOn = false;
            windParticle.Stop();
            col.enabled = false;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!blowerOn)
            return;

        if (other.CompareTag(BALL))
        {
            targetBody = other.GetComponent<Rigidbody>();
            float XX = transform.eulerAngles.x;
            if (XX > 0)
            {
                XX = Mathf.Abs(XX);
                float t = Mathf.InverseLerp(0f, 50f, XX);
                dirY = Mathf.Lerp(50f, 85f, t);
                
            }
            dir = other.transform.position - transform.position;
            dir.y = 0;
            Vector3 upforce = new Vector3(0f, dirY * upForceMult, 0f);
            targetBody.AddForce(dir * forceMult + upforce);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (!blowerOn)
            return;

        if (other.CompareTag(BALL))
        {
            targetBody = other.GetComponent<Rigidbody>();
            float XX = transform.eulerAngles.x;
            if (XX > 0)
            {
                XX = Mathf.Abs(XX);
                float t = Mathf.InverseLerp(0f, 50f, XX);
                dirY = Mathf.Lerp(50f, 85f, t);

            }
            dir = other.transform.position - transform.position;
            dir.y = 0;
            Vector3 upforce = new Vector3(0f, dirY * upForceMult, 0f);
            targetBody.AddForce(dir * forceMult + upforce, ForceMode.Acceleration);
        }
    }
}
