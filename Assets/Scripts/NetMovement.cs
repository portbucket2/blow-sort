using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class NetMovement : MonoBehaviour
{
    [SerializeField] Transform net;
    [SerializeField] int limit = 5;
    private BoxCollider col;
    private void Awake()
    {
        col = GetComponent<BoxCollider>();
    }
    void Start()
    {
        InvokeRepeating("NewPosition", limit, limit);
    }
    void NewPosition()
    {
        Vector3 pos = RandomPointInBounds(col.bounds);
        net.DOMove(pos, 1f).SetEase(Ease.OutSine);
    }

    public Vector3 RandomPointInBounds(Bounds bounds)
    {
        return new Vector3(
            Random.Range(bounds.min.x, bounds.max.x),
            Random.Range(bounds.min.y, bounds.max.y),
            Random.Range(bounds.min.z, bounds.max.z)
        );
    }
}
