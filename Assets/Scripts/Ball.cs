using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Ball : MonoBehaviour
{
    [SerializeField] BallColor color;
    private Rigidbody rb;
    private SphereCollider col;
    private MeshRenderer ren;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<SphereCollider>();
        ren = GetComponent<MeshRenderer>();
    }

    void Start()
    {
        
    }
    public void SetMaterial(Material mat)
    {
        ren.material = mat;
    }
    public BallColor GetColor()
    {
        return color;
    }
    public void Collect(Vector3 _pos)
    {
        rb.isKinematic = true;
        col.enabled = false;
        transform.DOMove(_pos, 0.5f).SetEase(Ease.OutSine);
    }
    public void CollectVanish(Vector3 _pos)
    {
        rb.isKinematic = true;
        col.enabled = false;
        transform.DOMove(_pos, 0.5f).SetEase(Ease.OutSine).OnComplete(Vanish);
    }
    void Vanish()
    {
        gameObject.SetActive(false);
    }
}
