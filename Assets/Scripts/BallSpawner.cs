using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawner : MonoBehaviour
{
    [SerializeField] GameObject ballPrefab;
    [SerializeField] int spawnNumber;
    [SerializeField] int spawnRadius;
    [SerializeField] float spawnHeight;
    [Header("Materials: ")]
    [SerializeField] List<Material> mats;

    private List<GameObject> balls;
    GameController gameController;
    GameManager gameManager;
    GameDataSheet data;
    void Start()
    {
        gameManager = GameManager.GetManager();
        data = gameManager.GetDataSheet();
        spawnNumber = data.levelDatas[gameManager.GetlevelCount()].spawnCount;
        gameController = GameController.GetController();
        gameController.SetTotalBallsSpwaned(spawnNumber);
        int max = mats.Count;
        balls = new List<GameObject>();
        Vector3 pos;
        for (int i = 0; i < spawnNumber; i++)
        {
            Vector2 rand = Random.insideUnitCircle * spawnRadius;
            pos = new Vector3(rand.x, spawnHeight, rand.y);
            GameObject gg = Instantiate(ballPrefab);
            gg.transform.localPosition = pos;

            gg.GetComponent<Ball>().SetMaterial(mats[Random.Range(0, max)]);
            gg.GetComponent<Rigidbody>().isKinematic = true;
            balls.Add(gg);
        }
        Invoke("Move", 0.3f);
    }

    void Move()
    {
        for (int i = 0; i < spawnNumber; i++)
        {
            balls[i].GetComponent<Rigidbody>().isKinematic = false;
        }
    }
}
