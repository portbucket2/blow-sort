using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController gameController;

    private void Awake()
    {
        gameController = this;
    }
    public static GameController GetController()
    {
        return gameController;
    }

    [SerializeField] UIController uiController;
    [SerializeField] InputController inputController;

    [Header("Time Scale: ")]
    [SerializeField] float timeScale = 1f;

    [Header("ball counts: ")]
    [SerializeField] int remainingBallCount;
    [SerializeField] int collectedBallCount;

    GameManager gameManager;
    GameDataSheet data;
    int targetBalls;
    AnalyticsController analytics;
    void Start()
    {
        gameManager = GameManager.GetManager();
        data = gameManager.GetDataSheet();
        targetBalls = data.levelDatas[gameManager.GetlevelCount()].tagetCount;
        analytics = AnalyticsController.GetController();
        SetTimeScale();
    }
    public UIController GetUIController() { return uiController; }


    void ShowBallCount()
    {
        uiController.UpdateCount(remainingBallCount, collectedBallCount);
    }
    void SetTimeScale()
    {
        Time.timeScale = timeScale;
        Time.fixedDeltaTime = 0.02F * Time.timeScale;
    }
    void LevelEndCheck()
    {
        if (collectedBallCount > targetBalls)
        {
            // level end
            Debug.Log("Level End");
            uiController.ShowLevelEnd();
            inputController.InputEnabled(false);
            analytics.LevelCompleted();
        }
    }
    public void AddBall(BallColor color)
    {
        //if (color == BallColor.Red)
        //{
        //    redBallCount++;
        //}
        //else if (color == BallColor.Green)
        //{
        //    greenBallCount++;
        //}
        //else if (color == BallColor.Blue)
        //{
        //    blueBallCount++;
        //}
        remainingBallCount--;
        collectedBallCount++;
        ShowBallCount();
        LevelEndCheck();
    }
    public void SetTotalBallsSpwaned(int count)
    {
        remainingBallCount = count;
        ShowBallCount();
    }
}
