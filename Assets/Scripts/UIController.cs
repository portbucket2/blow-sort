using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIController : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI redText;
    [SerializeField] TextMeshProUGUI greenText;

    [SerializeField] GameObject levelEndPanel;
    [SerializeField] Button levelEndButton;

    GameManager gameManager;
    int targetBalls;
    void Start()
    {
        gameManager = GameManager.GetManager();
        targetBalls = gameManager.GetDataSheet().levelDatas[gameManager.GetlevelCount()].tagetCount;
        levelEndButton.onClick.AddListener(delegate
        {
            gameManager.GotoNextStage();
        });
    }

    // Update is called once per frame
    public void UpdateCount(int remaining, int collected)
    {
        redText.text = "Target: " + collected + " / " + targetBalls;
        greenText.text = "Total Balls: " + remaining;
    }
    public void ShowLevelEnd()
    {
        levelEndPanel.SetActive(true);
    }
}
