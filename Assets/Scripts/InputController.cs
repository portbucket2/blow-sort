using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    [SerializeField] bool inputEnabled = true;
    [SerializeField] Transform blower;
    [SerializeField] float speed;
    [Header("Position Blower : ")]
    [SerializeField] Vector2 xMove;
    [SerializeField] Vector2 zMove;

    [Header("Rotation Blower: ")]
    [SerializeField] Vector2 xRot;
    [SerializeField] Vector2 yRot;
    [Header("Rotation camera: ")]
    [SerializeField] Vector2 yCam;
    [SerializeField] Transform camera;


    private Vector3 startPos;
    private Vector3 dir;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!inputEnabled)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            startPos = Input.mousePosition;
        }
        if (Input.GetMouseButton(0))
        {
            dir = startPos - Input.mousePosition;
            ChangeBlowerPosition(-dir);
        }
        if (Input.GetMouseButtonUp(0))
        {
            startPos = Input.mousePosition;
        }
    }

    void ChangeBlowerPosition(Vector3 _dir)
    {
        Vector3 dir = new Vector3(_dir.x, 0f, _dir.y);
        Vector3 newPos = blower.position + dir * speed;
        // x axis -6.5 to 6.5
        // z axis 0 to-18;
        float xAxis = newPos.x;
        float zAxis = newPos.z;
        xAxis = Mathf.Clamp(xAxis, xMove.x, xMove.y);
        zAxis = Mathf.Clamp(zAxis, zMove.x, zMove.y);
        newPos.x = xAxis;
        newPos.z = zAxis;
        
        blower.position = newPos;

        // rotation
        float YY = blower.position.x;
        float XX = blower.position.z;
        float angleY = 0f;
        float angleX = 50f;
        float angleCam = 0f;
        if (YY > 0)
        {
            YY = Mathf.Abs(YY);
            float t = Mathf.InverseLerp(0, 6, YY);
            angleY = Mathf.Lerp(yRot.x, yRot.y, t);
            angleY = -angleY;
            angleCam = Mathf.Lerp(yCam.x, yCam.y, t);
            angleCam = -angleCam;
        }
        else
        {
            YY = Mathf.Abs(YY);
            float t = Mathf.InverseLerp(0, 6, YY);
            angleY = Mathf.Lerp(yRot.x, yRot.y, t);
            //angle = -angle;
            angleCam = Mathf.Lerp(yCam.x, yCam.y, t);
        }
        if (XX < 0)
        {
            XX = Mathf.Abs(XX);
            float t = Mathf.InverseLerp(-1f, 18f, XX);
            angleX = Mathf.Lerp(xRot.x, xRot.y, t);
            //angleX = -angleX;
            //blower.eulerAngles = new Vector3(0f, angleY, 0f);
        }

        blower.eulerAngles = new Vector3(angleX, angleY, 0f);
        camera.eulerAngles = new Vector3(camera.eulerAngles.x, angleCam, camera.eulerAngles.z);
    }

    public void InputEnabled(bool isTrue)
    {
        inputEnabled = isTrue;
    }
}
